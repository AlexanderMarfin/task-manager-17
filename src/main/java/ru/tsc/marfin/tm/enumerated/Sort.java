package ru.tsc.marfin.tm.enumerated;

import ru.tsc.marfin.tm.comparator.CreatedComparator;
import ru.tsc.marfin.tm.comparator.DateStartComparator;
import ru.tsc.marfin.tm.comparator.NameComparator;
import ru.tsc.marfin.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_START("Sort by start date", DateStartComparator.INSTANCE);

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values()) {
            if(sort.name().equals(value)) return sort;
        }
        return null;
    }

    private final String displayName;

    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
